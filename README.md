# Prove That My Plugins Work

Just to prove that there is no issue with [Avatar-API](https://gitlab.com/avatar-tales/avatar-api).


## Compiling

To compile the plugin, run the following command:
```sh
./gradlew build
```

This will create a JAR file in the `build/libs` directory.
