package fr.alessevan.proveplugin.commands;

import fr.alessevan.proveplugin.ProveModule;
import fr.alessevan.api.commands.AvatarCommand;
import fr.alessevan.api.commands.RegisterCommand;
import fr.alessevan.api.hooks.defaults.IMoneyHook;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.List;

@RegisterCommand(
        name = "balance",
        description = "Show your money",
        usage = "/balance",
        permission = "balance.show",
        aliases = {"b", "bal"},
        maxArgs = 1,
        sender = RegisterCommand.Sender.ALL
)
public class BalanceCommand implements AvatarCommand {

    private final ProveModule module;

    public BalanceCommand(ProveModule module) {
        this.module = module;
    }

    @Override
    public boolean execute(CommandSender sender, String cmd, List<String> args) {
        if (args.isEmpty()) {
            if (sender instanceof Player player) {
                this.execute(player, cmd, args);
                return true;
            }
        }
        if (args.isEmpty()) {
            sender.sendMessage("You must specify a player!");
            return true;
        }
        Player player = this.module.getServer().getPlayer(args.get(0));
        if (player == null) {
            sender.sendMessage("Player not found!");
            return true;
        }
        this.module.api().getServiceHook(IMoneyHook.class)
                .ifPresentOrElse(
                        moneyHook -> sender.sendMessage("Money of " + player.getName() + ": " + moneyHook.getMoney(player)),
                        () -> sender.sendMessage("Money system is not enabled!")
                );
        return true;
    }

    @Override
    public boolean execute(Player player, String commandLabel, List<String> args) {
        this.module.api().getServiceHook(IMoneyHook.class)
                .ifPresentOrElse(
                        moneyHook -> player.sendMessage("Your money: " + moneyHook.getMoney(player)),
                        () -> player.sendMessage("Money system is not enabled!")
                );
        return true;
    }

    @Override
    public boolean execute(ConsoleCommandSender sender, String commandLabel, List<String> args) {
        return this.execute((CommandSender) sender, commandLabel, args);
    }

    @Override
    public List<String> suggest(CommandSender sender, String commandLabel, List<String> args) {
        if (args.size() <= 1)
            return this.module.getServer().getOnlinePlayers().stream().map(Player::getName).toList();
        return List.of();
    }
}
