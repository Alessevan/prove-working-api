package fr.alessevan.proveplugin;

import fr.alessevan.api.AvatarAPI;
import fr.alessevan.api.module.AvatarModule;
import fr.alessevan.api.module.RegisterModule;
import fr.alessevan.proveplugin.commands.BalanceCommand;
import fr.alessevan.proveplugin.launcher.ProveLaunch;
import fr.alessevan.proveplugin.listeners.ConnectionHandler;

import java.nio.file.Path;

@RegisterModule(
        name = "ProveModule",
        version = "1.0",
        author = "Alessevan"
)
public class ProveModule extends AvatarModule<ProveLaunch> {

    public ProveModule(ProveLaunch plugin, AvatarAPI api, Path dataFolder) {
        super(plugin, api, dataFolder);
    }

    @Override
    public void onEnable() {
        this.registerCommands(new BalanceCommand(this));
        this.registerEvents(new ConnectionHandler(this));
        this.logger().info("ProveModule has been enabled!");
    }

    @Override
    public void onDisable() {
        this.logger().info("ProveModule has been disabled!");
    }
}