package fr.alessevan.proveplugin.listeners;

import fr.alessevan.proveplugin.ProveModule;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class ConnectionHandler implements Listener {

    private final ProveModule module;

    public ConnectionHandler(ProveModule module) {
        this.module = module;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        event.getPlayer().sendMessage("Welcome to the server!");
        this.module.getServer().getScheduler()
                .runTaskLater(
                        this.module.plugin(),
                        () -> event.getPlayer().playSound(
                                net.kyori.adventure.sound.Sound.sound(
                                        Sound.EVENT_RAID_HORN,
                                        net.kyori.adventure.sound.Sound.Source.MASTER,
                                        12.0f,
                                        1.0f
                                ),
                                net.kyori.adventure.sound.Sound.Emitter.self()
                        ),
                        20L
                );
    }

}
