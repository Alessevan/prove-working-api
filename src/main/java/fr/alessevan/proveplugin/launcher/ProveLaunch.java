package fr.alessevan.proveplugin.launcher;

import fr.alessevan.proveplugin.ProveModule;
import fr.alessevan.api.AvatarAPI;
import org.bukkit.plugin.java.JavaPlugin;

public class ProveLaunch extends JavaPlugin {

    @Override
    public void onEnable() {
        AvatarAPI.getAPI().registerModule(this, ProveModule.class);
        this.getLogger().info("ProveModule has been registered!");
    }

    @Override
    public void onDisable() {
        AvatarAPI.getAPI().unregisterModule(ProveModule.class);
        this.getLogger().info("ProveModule has been unregistered!");
    }

}
